from django.conf.urls import url

from.views import login, logout, register

app_name = 'simbion_auth'

urlpatterns = [
  url(r'^login/$', login, name='login'),
  url(r'^logout/$', logout, name='logout'),
  url(r'^register/$', register, name='register'),
]
