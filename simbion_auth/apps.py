from django.apps import AppConfig


class SimbionAuthConfig(AppConfig):
    name = 'simbion_auth'
