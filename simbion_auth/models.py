from django.db import models

# Create your models here.
class Pengguna(models.Model):
  username = models.CharField('username', max_length=20, primary_key=True)
  password = models.CharField('password', max_length=20)
  role = models.CharField('role', max_length=20)

class Donatur(models.Model):
  nomor_identitas = models.CharField('nomor_identitas', max_length=20, primary_key=True)
  email = models.CharField('email', max_length=50)
  nama = models.CharField('nama', max_length=50)
  npwp = models.CharField('npwp', max_length=20)
  no_telp = models.CharField('no_telp', max_length=20, null=True)
  alamat = models.CharField('alamat', max_length=50)
  username = models.ForeignKey(Pengguna, to_field='username', on_delete=models.CASCADE)

class Individual(models.Model):
  nik = models.CharField('nik', max_length=16, primary_key=True)
  nomor_identitas_donatur = models.ForeignKey(Donatur, to_field='nomor_identitas', on_delete=models.CASCADE)

class Yayasan(models.Model):
  no_sk_yayasan = models.CharField('no_sk_yayasan', max_length=20, primary_key=True)
  email = models.CharField('email', max_length=50)
  nama = models.CharField('nama', max_length=50)
  no_telp_cp = models.CharField('no_telp_cp', max_length=20, null=True)
  nomor_identitas_donatur = models.ForeignKey(Donatur, to_field='nomor_identitas', on_delete=models.CASCADE)

class Mahasiswa(models.Model):
  npm = models.CharField('npm', max_length=20, primary_key=True)
  email = models.CharField('email', max_length=50)
  nama = models.CharField('nama', max_length=50)
  no_telp = models.CharField('no_telp', max_length=20, null=True)
  alamat_tinggal = models.CharField('alamat_tinggal', max_length=50)
  alamat_domisili = models.CharField('alamat_domisili', max_length=50)
  nama_bank = models.CharField('nama_bank', max_length=50)
  no_rekening = models.CharField('no_rekening', max_length=20)
  nama_pemilik = models.CharField('nama_pemilik', max_length=20)
  username = models.ForeignKey(Pengguna, to_field='username', on_delete=models.CASCADE)

class Admin(models.Model):
  username = models.ForeignKey(Pengguna, to_field='username', on_delete=models.CASCADE)
