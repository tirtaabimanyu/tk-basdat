from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from .models import Pengguna, Donatur, Individual, Yayasan, Mahasiswa
# Create your views here.
def login(request):
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']
    user = Pengguna.objects.get(username=username, password=password)
    if user is not None:
      request.session['user'] = username
      request.session['role'] = user.role
      return redirect('simbion_beasiswa:index')
    else:
      messages.error(request, 'Username tidak terdaftar / Password salah')
      return redirect('simbion_auth:login')
  else:
    return render(request, 'auth/login.html')

def logout(request):
  request.session.flush()
  return redirect('simbion_beasiswa:index')

def register(request):
  if request.method == 'POST':
    print(request.POST)
    if request.POST['role'] == 'donatur':
      if request.POST['tipe'] == 'individu':
        user = Pengguna.objects.create(
          username = request.POST['username_individu'],
          password = request.POST['password_individu'],
          role = request.POST['role']
        )
        donatur = Donatur.objects.create(
          nomor_identitas = request.POST['nomor_identitas_individu'],
          email = request.POST['email_individu'],
          nama = request.POST['nama_individu'],
          npwp = request.POST['npwp'],
          no_telp = request.POST['no_telp_individu'],
          alamat = request.POST['alamat_individu'],
          username = user
        )
        individu = Individual.objects.create(
          nik = request.POST['nik'],
          nomor_identitas_donatur = donatur
        )
      elif request.POST['tipe'] == 'yayasan':
        user = Pengguna.objects.create(
          username = request.POST['username_yayasan'],
          password = request.POST['password_yayasan'],
          role = request.POST['role']
        )
        donatur = Donatur.objects.create(
          nomor_identitas = request.POST['nomor_identitas_yayasan'],
          email = request.POST['email_yayasan'],
          nama = request.POST['nama_yayasan'],
          npwp = request.POST['npwp'],
          no_telp = request.POST['no_telp_cp'],
          alamat = request.POST['alamat_yayasan'],
          username = user
        )
        yayasan = Yayasan.objects.create(
          no_sk_yayasan = request.POST['no_sk_yayasan'],
          email = request.POST['email_yayasan'],
          nama = request.POST['nama_yayasan'],
          no_telp_cp = request.POST['no_telp_cp'],
          nomor_identitas_donatur = donatur
        )
    elif request.POST['role'] == 'mahasiswa':
      user = Pengguna.objects.create(
        username = request.POST['username_mahasiswa'],
        password = request.POST['password_mahasiswa'],
        role = request.POST['role']
      )
      mahasiswa = Mahasiswa.objects.create(
        npm = request.POST['npm'],
        email = request.POST['email_mahasiswa'],
        nama = request.POST['nama_mahasiswa'],
        no_telp = request.POST['no_telp_mahasiswa'],
        alamat_tinggal = request.POST['alamat_tinggal'],
        alamat_domisili = request.POST['alamat_domisili'],
        nama_bank = request.POST['nama_bank'],
        no_rekening = request.POST['no_rekening'],
        nama_pemilik = request.POST['nama_pemilik'],
        username = user
      )
    return redirect('simbion_auth:login')
  else:
    return render(request, 'auth/register.html')
