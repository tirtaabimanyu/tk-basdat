from django.apps import AppConfig


class SimbionBeasiswaConfig(AppConfig):
    name = 'simbion_beasiswa'
