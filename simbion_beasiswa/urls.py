from django.conf.urls import url

from.views import index, new, new_beasiswa_paket, add_new_beasiswa, lihat

app_name = 'simbion_beasiswa'

urlpatterns = [
  url(r'^$', index, name='index'),
  url(r'^new$', new, name='new'),
  url(r'^new-paket$', new_beasiswa_paket, name='new_beasiswa_paket'),
  url(r'^add-beasiswa$', add_new_beasiswa, name='add_new_beasiswa'),
  url(r'^lihat$', lihat, name='lihat'),
]
