from django.shortcuts import render, redirect
from .models import Skema_Beasiswa, Syarat_Beasiswa, Skema_Beasiswa_Aktif
from simbion_auth.models import Donatur
# Create your views here.
response = {}
def index(request):
  response['role'] = request.session['role'] if 'role' in request.session else 'guest'
  return render(request, 'home/index.html', response)

def new(request):
  response['role'] = request.session['role'] if 'role' in request.session else 'guest'
  if response['role'] != 'donatur':
    return redirect('simbion_beasiswa:index')
  return render(request, 'home/membuat_beasiswa_baru.html')

def new_beasiswa_paket(request):
  response['role'] = request.session['role'] if 'role' in request.session else 'guest'
  if response['role'] != 'donatur':
    return redirect('simbion_beasiswa:index')

  if request.method == 'POST':
    donatur = Donatur.objects.get(username=request.session['user'])
    skema_beasiswa = Skema_Beasiswa.objects.create(
      kode = request.POST['kode'],
      nama = request.POST['nama'],
      jenis = request.POST['jenis'],
      deskripsi = request.POST['deskripsi'],
      nomor_identitas_donatur = donatur
    )
    syarat = Syarat_Beasiswa.objects.create(
      syarat = request.POST['syarat'],
      kode_beasiswa = skema_beasiswa
    )
    return redirect('simbion_beasiswa:index')
  elif request.method == 'GET':
    return render(request, 'home/membuat_paket_beasiswa.html')

def add_new_beasiswa(request):
  response['role'] = request.session['role'] if 'role' in request.session else 'guest'
  if response['role'] != 'donatur':
    return redirect('simbion_beasiswa:index')

  response['kodes'] = Skema_Beasiswa.objects.all()

  if request.method == 'POST':
    beasiswa = Skema_Beasiswa.objects.get(kode=request.POST['kode'])
    skema_beasiswa_aktif = Skema_Beasiswa_Aktif.objects.create(
      no_urut = request.POST['nomor'],
      tgl_mulai_pendaftaran = request.POST['mulai'],
      tgl_tutup_pendaftaran = request.POST['tutup'],
      kode_skema_beasiswa = beasiswa,
    )
    return redirect('simbion_beasiswa:index')
  elif request.method == 'GET':
    return render(request, 'home/tambah_beasiswa_baru.html', response)

def lihat(request):
  response['role'] = request.session['role'] if 'role' in request.session else 'guest'
  
  if response['role'] == 'guest':
    return redirect('/')

  response['info_beasiswa'] = Skema_Beasiswa_Aktif.objects.all()

  return render(request, 'home/lihat_beasiswa.html', response)
