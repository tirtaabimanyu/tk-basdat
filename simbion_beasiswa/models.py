from django.db import models

from simbion_auth.models import Pengguna, Donatur

# Create your models here.
class Skema_Beasiswa(models.Model):
  kode = models.IntegerField('kode', unique=True)
  nama = models.CharField('nama', max_length=50)
  jenis = models.CharField('jenis', max_length=20)
  deskripsi = models.CharField('deskripsi', max_length=50)
  nomor_identitas_donatur = models.ForeignKey(Donatur, to_field='nomor_identitas', on_delete=models.CASCADE)

class Syarat_Beasiswa(models.Model):
  syarat = models.CharField('syarat', max_length=50)
  kode_beasiswa = models.ForeignKey(Skema_Beasiswa, to_field='kode', on_delete=models.CASCADE)

class Skema_Beasiswa_Aktif(models.Model):
  no_urut = models.IntegerField('no_urut')
  tgl_mulai_pendaftaran = models.DateField()
  tgl_tutup_pendaftaran = models.DateField()
  periode_penerimaan = models.CharField('periode_penerimaan', max_length=50, null=True)
  status = models.CharField('status', max_length=20, null=True)
  jumlah_pendaftar = models.IntegerField(null=True)
  kode_skema_beasiswa = models.ForeignKey(Skema_Beasiswa, to_field='kode', on_delete=models.CASCADE)
