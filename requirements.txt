Django==2.0.5
django-admin==1.2.4
django-excel-response2==2.0.8
django-six==1.0.4
pytz==2018.4
screen==1.0.1
xlwt==1.3.0
